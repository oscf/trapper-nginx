upstream trapper_server {
    ip_hash;
    server trapper:8000;
}

server {
    listen 80;
    listen [::]:80 ipv6only=on;
    server_name $DOMAIN_NAME;	
    
    client_max_body_size 4G;
    sendfile on;

    # Match X-Accel-Redirect URL in /remote_download/$proto/$host/$path format
    # proto - http or https
    # host - remote storage host
    # path - file download path with all required authorization tokens
    location ~ ^/remote_download/(.*?)/(.*?)/(.*) {
        # Only allow internal redirects
        internal;

        # Docker DNS resolver
        resolver 127.0.0.11 ipv6=off;
        
        # Extract the remote URL parts
        set $download_protocol $1;
        set $download_host $2;
        set $download_path $3;
        # Reconstruct the remote URL
        set $download_url $download_protocol://$download_host/$download_path;
        
        # Remote storage host (Azure, S3, etc.)
        proxy_set_header Host $download_host;

        # Clear Authorization header and Cookie headers for security reasons
        proxy_set_header Authorization '';
        proxy_set_header Cookie '';
        
        # Allow setting Context-Disposition by Django Backend
        proxy_hide_header Content-Disposition;
        add_header Content-Disposition $upstream_http_content_disposition;

        # Drop Set-Cookie header from storage backend
        proxy_hide_header Set-Cookie;
        
        proxy_max_temp_file_size 0;

        proxy_pass $download_url$is_args$args;

        # Handle redirects internally to avoid revealing them to users
        proxy_intercept_errors on;
        error_page 301 302 307 = @handle_redirect;
    }

    # Redirection handlers for remote downloads
    location @handle_redirect {
        # Docker DNS resolver
        resolver 127.0.0.11 ipv6=off;
        
        set $saved_redirect_location '$upstream_http_location';
        proxy_pass $saved_redirect_location;
    }

    location /media/ {
        alias /app/media/;
    }

    location /static/ {
        alias /app/static/;
    }

    location /media/protected/ {
        alias /app/media/protected/;
        internal;
    }

    location /external_media/ {
        alias /app/external_media/;
        internal;
    }

    location / {
        proxy_pass http://trapper_server;
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_redirect off;
    }
}
